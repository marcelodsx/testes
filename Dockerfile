# Cria Imagem a partir da distro Alpine
FROM alpine:3.9

# Instala e configura o Python
RUN echo "**** install Python ****" && \
    apk add --no-cache python3 && \
    if [ ! -e /usr/bin/python ]; then ln -sf python3 /usr/bin/python ; fi && \
    \
    echo "**** install pip ****" && \
    python3 -m ensurepip && \
    rm -r /usr/lib/python*/ensurepip && \
    pip3 install --no-cache --upgrade pip setuptools wheel && \
    if [ ! -e /usr/bin/pip ]; then ln -s pip3 /usr/bin/pip ; fi

# Define o Diretório de Trabalho para "/app"
WORKDIR /app

# Copia o conteúdo do diretório corrente dentro do container em "/app"
COPY . /app

# Instala os pacotes necessários e definidos em "requisitos.txt" (Flask)
RUN pip install --trusted-host pypi.python.org -r requisitos.txt
#RUN pip install --upgrade pip

# Disponibiliza a Porta 8080 para fora do container
EXPOSE 8080

# Define variáveis de ambiente
ENV NAME World

# Executa "devops-server.py" quando o container for executado
CMD ["python", "devops-server.py"]