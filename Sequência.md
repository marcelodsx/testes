### Sequência:

# This is deployed to the cluster with the command 
kubectl create -f deployment.yaml

# As it's a Deployment object, a list of all the deployed objects can be obtained via
kubectl get deployment

# Details of individual deployments can be outputted with 
kubectl describe deployment webapp1

# All Kubernetes objects are deployed in a consistent way using kubectl. Deploy the Service with 
kubectl create -f service.yaml

# As before, details of all the Service objects deployed with 
kubectl get svc

# By describing the object it's possible to discover more details about the configuration
kubectl describe svc webapp1-svc.

# Para descobrir o caminho para acessar o serviço
minikube service <SERVICE NAME> --url

# Executar o Serviço na Linha de Comando
curl $(minikube ip):30080/devops

#Updates to existing definitions are applied using kubectl apply. To scale the number of replicas, deploy the updated YAML file using
kubectl apply -f deployment.yaml

# Instantly, the desired state of our cluster has been updated, viewable with 
kubectl get deployment

# Additional Pods will be scheduled to match the request. 
kubectl get pods

# As all the Pods have the same label selector, they'll be load balanced behind the Service NodePort deployed.
Issuing requests to the port will result in different containers processing the request curl host01:30080

# Additional Kubernetes Networking details and Object Definitions will will be covered in future scenarios.

## Ingress:
https://kubernetes.io/docs/tasks/access-application-cluster/ingress-minikube/