# Procedimento

##_ 1. Criação do Container com Aplicação em Python (fornecida) - No Docker Hub foi criado o Repositório "marcelodsx/testes"
	docker build -t testes .

##_ 2. Executar o Container criado mapeando a Porta 4001 para 8080
	docker run -p 4001:8080 testes

##_ 3. Efetua o Login no Docker
	docker login

##_ 4. Coloca uma "Tag" na Imagem
	docker tag <image> marcelodsx/testes:1.0

##_ 5.  Faz o Upload da Imagem para o Repositório Remoto do Docker
	docker push marcelodsx/testes:1.0

##_ 6. Executar o Container do Projeto
	docker run -p 4001:8080 marcelodsx/testes:1.0
		Retorna:
						{
  						"uuid": "71477d86-afba-400e-8766-40bbeccdba35"
						}
						No Browser
    Retorna:
    				172.17.0.1 - - [09/Jul/2019 01:25:18] "GET /devops HTTP/1.1" 200 -
    				No Terminal

##_ 7. Criado o Repositório "testes" no Gitlab

##_ 8. No Repositório "testes" do Gitlab foi criado o arquivo ".gitlab-ci.yml"

##_ 9. Criei arquivos simples em Python para mostrar o processo de CI, já que o arquivo Python do Desafio mantém o serviço ativo e o pipeline não avança

##_ 10.Imagem no Registry do Gitlab
	docker login registry.gitlab.com/marcelodsx/testes -u marcelodsx -p <deploy-token>
		Retorna:
						Login Succeeded
	docker build -t registry.gitlab.com/marcelodsx/testes .
	docker image ls -a
	docker tag ad1346 registry.gitlab.com/marcelodsx/testes:1.1
	docker push registry.gitlab.com/marcelodsx/testes
		Retorna:
						denied: requested access to the resource is denied (VI QUE É UM ERRO RECORRENTE DO GITLAB) - https://gitlab.com/gitlab-com/support-forum/issues/2370
	
	docker pull marcelodsx/testes:1.1
		e
	docker run -p 4001:8080 marcelodsx/testes:1.1
		estão funcionando
	
	Solucionado, após algumas horas de tentativas e pesquisas, com:
	1. I removed "~/.docker/config.js";
	2. I removed the 2FA from my account;
	3. Then, I try a "docker login registry.gitlab.com" and it works again;
	4. After that, the "docker push registry.gitlab.com/marcelodsx/testes:1.2" works.

##_ 11.Criada uma imagem, Desafio:2.1 para seguir com o Desafio

##_ 12.